# CRAPS

# OFF

- DON’T PASS - coming out:  2 / 3
     - 12 is a Push
- PASS LINE - coming out:  7 / 11

# ON

- DON’T PASS must hit 7 before Point
- PASS LINE must hit Point before 7

## DOUBLE DOWN

- DON’T PASS - 4 or 10, others pull your money
- PASS LINE - 6 or 8

## NEVER BET MIDDLE

## FIELD - Sucker Bet - Sometimes Lucky

## ODDS

|2 / 12|3 / 11|4 / 10|5 / 9|6 / 8|7|
|--|--|--|--|--|--|
|1|2|3|4|5|6|

![Craps Table](crapsTable.svg)
