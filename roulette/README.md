# Roulette Computation

The History Channel had a story about two students, the year 1977 majoring in Physics.  There names were Norman Packard and Doyne Farmer and their company name was Eudaemonics.  They came up with an equation to compute which 1/8 segment of a Roulette wheel the ball would land.  They went to Vegas to prove their paper.

Here is the calculation that I believe is correct:

![Roulette](roulette.gif)
