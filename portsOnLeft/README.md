# Laptop Computers - Port Side with Cords

- The World's population is 90% Right Handed, almost every tool, game is developed for a Right Handed person
- Why do Computer Manufactures not put all Peripherals with Cords on the left side of the computer

## Bottom Line

- Put the Peripherals with Cords on the Left
- If we cannot compromise, then put the Peripherals with Cords in the Back
- STOP Interfering with the User

## Wrong

- Cables are in the way of the 90% of computer users

|Wrong 1|Wrong 2|
|--|--|
|![Wrong1](wrong1.jpg)|![Wrong2](wrong2.jpg)|

## Right

- Cables are NOT in the way of the 90% of computer users

|Right 1|Right 2|
|--|--|
|![Right1](right1.jpg)|![Right2](right2.jpg)|
