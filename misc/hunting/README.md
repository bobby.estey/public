# Hunting Kit Contents

|Item|Item|
|--|--|
|Small First Aid Kit|Water Bottles|
|Small Tin Cup|Bright Silk Fabric|
|Mini Flair Kit|Small Hand Held Mirror|
|Small Flashlight|Glow Stick|
|Disposable Lighter|Waterproof Matches|
|Emergency Blanket|Fire Starter|
|Garbage Bags|Comb|
|#500 Parachute String|Plastic Parka / Poncho|
|Plastic Gloves|Flair Gun (Army)|
|Pilot Survival Kit (Army)|Maps|
|Plastic Drinking Straw s|Light Dehydrated Foods|
|Pack Saw|Folding Knife|
|F Napkins|Hand Warmers|
|2 Compases|Ace Bandage|
|Whistle|Hard Candy|
|Surveyor Tape|Maker Tape|
|GI Joe Quick Light (Army)|Canteen|
|Cell Phone|GPS|
 