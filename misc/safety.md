# WARNING:  HOTELS COULD BE HAZARDOUS TO YOUR HEALTH...

- by Captain RH Kauffman, Los Angeles County Fire Department


Have you ever been in a hotel during a fire?  It’s a frightening experience, and you should start thinking about it.  For instance, how would you have acted if you had been in one of these fires?

The Thomas Hotel, San Francisco, Ca	20 DEAD
The Gulf Hotel, Houston, Texas	54 DEAD
The La Salle Hotel, Chicago, Ill	61 DEAD
The Wincoff Hotel, Atlanta, Ca	119 DEAD

Of course, there have been hundreds more with thousands of deaths, but I think you’re getting the drift.  The majority of those people did not have to die.

My wife has been in the airline industry close to 8 years and while accompanying her on a trip recently, I learned how ill-prepared she was for a hotel fire.  It’s not her fault:  its’ quite common.  Hotels, however, have no excuse for being ill-prepared, but believe me, you cannot depend on the staff in case of a fire.  History has shown some hotels won’t even call the fire Department.  I have been a fire-fighter in Los Angeles for over 10 years and have seen many people die needlessly in building fires.  It’s sad because most could have saved themselves.


What you’re about to read is roughly the same “briefing” I have given my wife on hotel safety.  I do not intend to “play down” the aspects of hotel fires or soft soap the language.  It’s critical that you remember how to react, and, if I shake you a little, maybe you will.

Contrary to what you have seen on television or in the movies, fire is not likely to chase you down and burn you to death.  It’s the bi-products of fire that will kill you.  Super heated fire gases (smoke) and panic will almost always be the cause of death long before the fire arrives if it ever does.   This is very important.  You must know how to avoid smoke and panic to survive a hotel fire.   With this in mind, here are a few tips:

## SMOKE

Where there is smoke, there is not necessarily fire.  A smoldering mattress, for instance, will produce great amounts of smoke.  Air conditioning and air exchange systems will sometimes pick up smoke from one room and carry it out to other rooms or floors.  You should keep that in mind because 70% of the hotel fires are caused by smoking and matches.  In any case, your prime objective should be to leave at the first sign of smoke.

Smoke, being warmer, will start accumulating at the ceiling and work its way down.  The first thing you will notice is THERE ARE NO “EXIT” SIGNS.  I’ll talk more about the exits later, just keep in mind when you have smoke, it’s too late to start looking for “exit” signs.

Another thing about smoke you should be aware of is how irritating it is on the eyes.  The problem is your eyes will only take so much irritation then they close.  Try all you want, you won’t be able to open them if there is still smoke in the area.  It’s one of your body’s compensatory mechanisms.  Lastly, the fresh air you want to breath is at or near the floor.  Get on your hands and knees (or stomach) and STAY THERE as you make you way out.  Those who don’t probably won’t get far.

Think about this poor man’s predicament for a moment:

He wakes up at 0230 hrs to a smell of smoke.  He puts on his trousers and runs into the hallway only to be greeted by heavy smoke.  He has no idea where the exit is.  He runs to the right.  He’s coughing and gagging, his eyes hurt.  “Where is it?”  “WHERE IS IT?”  Panic begins to set in.  About the same time he thinks maybe he is going the wrong way, his eyes close.  He can’t find his way back to his room (it wasn’t so bad in there).  His chest hurts, he desperately needs oxygen.  Total panic sets in as he runs in the other direction.  He is completely disorientated.  He cannot hold his breath any longer.  We find him at 0250.  DEAD

What caused all the smoke?  A small fire in a room where they store the roll-away beds.   Remember, the presence of smoke does not necessarily mean the hotel is burning down.

## PANIC

Panic (pan ik).  A sudden, overpowering terror often afflicting many people at once.  Panic is the product of your imagination running wild and it will set in as soon as it dawns on you you’re lost, disorientated, or you don’t know what to do.  Panic is almost irreversible:  once it sets in, it seems to grow.  Panic will make you do things that could kill you.  People in a state of panic are rarely able to save themselves.

If you understand what’s going on, what to do, where to go, and how to get there, panic will not set in.  The man in the example I used would not have died if he had known what to do.  For instance, had he known the exit was to the left and 4 doors down on the left, he could have gotten on his hands and knees where there was fresh air and started counting doorways.  Even if he couldn’t keep his eyes open, he could feel his way as he crawled, counting the doors.  1... 2... 3... BINGO!  He would NOT have panicked.  He would be alive today, telling of his “great hotel fire” experience.

## EXITS

The elevator drops you at the 12th floor and you start looking for your room.  “Let’s see ... room 1236 ... here it is”.  You open the door and drop your luggage.  AT THAT VERY MOMENT, turn around and go back into the hallway to check your exit.  You may NEVER get another chance.  Don’t go into the bathroom, open the curtains, turn on the TV, smarten your appearance, or crash out on the bed.  I know you’re tired and you want to relax, but it’s absolutely essential ... no ... CRITICAL that you develop the HABIT of checking for your exit after you drop your luggage.  It won’t take 30 seconds, and believe me, you may NEVER get another chance.

If there are 2 of you sharing a room, BOTH of you locate your exit.  Talk it over as you walk towards it.  Is it on the left or right ... do you have to turn a corner?  Open the exit door ... what do you see ... stairs or another door?  (Sometimes there are 2 doors to go through, especially in newer hotels.  ) I’d hate to see you crawl into a broom closet thinking it was the exit!  Are you passing any rooms where your friends are staying?  If there was a fire, you may want to bang on their doors as you go by.  Is there anything in the hallway that would be in your way ... an ice-machine maybe?  As you arrive back at your room, take a look once more.  Get a good mental picture of what everything looks like.  Do you think you could get to the exit with a “blindfold” on?

This procedure takes less than one minute and to be effective, it must become a habit.  Those of you who are too lazy or tired to do it consistently are real “riverboat gamblers”.  There are over 5,000 hotel fires per year.  The odds are sure to catch up with you.

## USING THE EXIT

Should you have to leave your room during the night, it is important to close the door behind you.  This is very effective in keeping out fire and will minimise smoke damage to you belongings.

There was a house fire in Los Angeles recently where an entire family died.  It was a 3 bedroom house with a den and family room.  That night, the occupants had left every door in the house open except one, and it had led to the washrooms where the family dog slept.  The house, except for the washroom, was a total loss.  When the fire was knocked down, firemen opened the door to find the family dog wagging his tail.  Because the door was left shut, the dog and room were in fine shape.

Some doors take hours to burn through.  They are excellent “fire stops” so close every door you go through.  If you find smoke in the exit stairwell, you can bet people are leaving the doors open as they enter.

Always take your key with you.  Get into the habit of putting the key in the same place every time you stay in a hotel.  Since every hotel has night stands, that’s an excellent location.  It’s close to the bed so you can grab it when you leave without wasting time looking for it.  It’s important you close your door as you leave, and it’s equally as important that you don’t lock yourself out.  You may find conditions in the hallway untenable, and want to return to your room.  If you’re now in the habit of checking your exit and leaving the room key on the night stand, you’re pretty well prepared to leave the hotel in case of a fire, so let’s “walk” through it once.

Something will awake you during the night.  It could be the telephone, someone banging on the door, the smell of smoke, or some other disturbance.  But, whatever it is, investigate it before you go back to sleep.  A popular “Inn” near LAX recently had a fire and one of the guests later said he was awakened by people screaming but went back to bed thinking it was a party.  He dammed near died in bed.

Let’s suppose you wake up to smoke in your room.  Grab you key off the night stand, roll off the bed and head for the door on you hands and knees.  Even if you could tolerate the smoke by standing, DON’T.  You’ll want to save your eyes and lungs for as long as possible.  BEFORE you open the door, feel it with the palm of your hand.  If the door or knob is quite hot, don’t open it.  The fire could be just outside.  We’ll talk about that later.  With the palm of your hand still on the door (in case you need to slam it shut), slowly open the door and peek into the hallway to “assess conditions”.

As you make your way to the exit, stay against the wall on the side where the exit is.  It is very easy to get lost or disorientated in a smoky atmosphere.  If you’re on the wrong side of the hallway, you might crawl right on by the exit.  If you’re in the middle of the hall, people who are running will trip over you.  Stay on the same side as the exit, count doors as you go.

When you reach the exit and begin to descend it is very important that you WALK down and hang onto the handrail as you go.  Don’t take this point lightly.  The people who will be running will knock you down and you might not be able to get up.  Just hang on and stay out of everyone’s way.  All you have to do now is leave the building, cross the street and watch the action.  When the fire is out and the smoke clears, you will be allowed to re-enter the building.  If you closed your room door when you left, you belongings should be in pretty good shape.
Smoke will sometimes get into the exit stairway.  It it’s a tall building, this smoke may not rise very high before it cools and becomes heavy.  This is called “stacking”.  If your room is on the 20th floor, for instance, you could enter the stairway and find it clear.  As you descend you could encounter smoke that has  “stacked”.  Do not try to “run through it” - people die that way.  Turn around and walk up.  Now you must really hang onto the handrail.  The people running down will probably be glassy-eyed and in a panic and will knock you right out of your socks!

They will run over anything in their way, including a fireman.  You’ll feel as though you’re going upstream against the Chicago Bears, but hang on and keep heading up towards the roof.  If for some reason you try one of the doors to an upper floor and find it locked, that’s normal, don’t worry about it.  Exit stairwells are designed so that you cannot enter from the street or roof.  Once inside, however, you may exit at the street or roof but cannot go from floor to floor; this is done for security purposes.  When you reach the roof, prop the door with something.  This is the ONLY time you will leave a door open.  Any smoke in the stairwell may now vent itself to the atmosphere and you won’t be locked out.  Now find the windward side of the building (the wet finger method is quite reliable), have a seat and wait until they find you.  Roofs have proved to be a safe secondary exit and refuge area.  Stay put.  Firemen will always make a thorough search of the building looking for bodies.  Live ones are nice to find.

## YOUR ROOM

After you check your exit and drop the key on the night stand, there is one more thing for you to do.  Become familiar with your room.  See if your bathroom has a vent; all do, but some have electric motors.  Should you decide to remain in your room, turn it on to help remove the smoke.  Take a good look at the window in your room.  Does it open?  Does it have a latch, a lock?  Does it slide?  Now open the window (if it works) and look outside.  What do you see?  A sign, ledges?  How high up are you?  Get a good mental picture of what’s outside, it may come in handy.  It’s important you know how to OPEN your window, you may have to close it again.

Should you wake up to smoke in your room and the door is too hot to open or the hallway is completely charged with smoke, don’t panic.   Many people have defended themselves quite nicely in their room and so can you.  One of the first things you’ll want to do is open the window to vent the smoke.  I hope you learned how to open it when you checked in.  It could be dark and smoking in the room.  Those who don’t will probably throw a chair through the window.  If there is smoke outside and you have no window to close, it will enter your room and you will be trapped.  The broken glass from the window will cut like a surgeon’s scalpel.  At the Ramada Inn fire, an airline captain on a layover threw a chair through the window and cut himself seriously.  Don’t compound your problems.  Besides, if you break out your window with a chair, you could hit a fireman on the street below.

If there is fresh air outside, leave the window open, but keep an eye on it.  At this point, most people would stay at the window, waving frantically, while their room continues to fill with smoke, if the fire burns through.  This procedure is not conducive to longevity.  You must be aggressive and fight back.  Here are some things you can do in any order you choose ... if the room phone works, let someone know you’re in there.  Flip on the bathroom vent.  Fill the bath with water.  (Don’t get into it - it’s for fire fighting.  You’d be surprised how many people try to save themselves by getting into a tub of water - that’s how you cook lobsters and crabs, so you know what happens!)   Wet some sheets or towels, and stuff the cracks of your door to keep out the smoke.  With your ice-bucket, bail the water from the bath onto the door to keep it cool.  Feel the walls - if they are hot, bail water onto them too.  You can put your mattress up against the door and block it in place with the dresser.  Keep it wet - keep everything wet.  Who cares about the mess.  A wet towel tied around your nose and mouth is an effective filter if your fold it in a triangle and put the corner in your mouth.  If you swing a wet towel around the room, it will help clear the smoke.  If there is a fire outside the window, pull down the curtains and move everything combustible away from the window.  Bail water all around the window.  Use your imagination and you may come up with some tricks of you own.  The point is, there shouldn’t be any reason to panic - keep fighting until reinforcements arrive.  It won’t be long.

## ELEVATORS

There isn’t an elevator made that can be used as a “safe” exit.  In all  states, elevators by law, cannot be considered an “exit”.  They are complicated devices with a mind of their own.  The problem is people only know one way out of a building - the way they came in, and if that was the elevator, they are in trouble.  Elevator shafts and machinery extends through all floors of a building, and besides, with the shaft filling with smoke, there are hundreds of other things that could go wrong and probably will.  Everyone tries to get on the elevator in an emergency.  Fights break out and people get seriously injured.  Smoke, heat and fire do funny things to elevator call buttons, controls and other complicated parts.  Case in point:

Hotel guests in a New Orleans hotel were called on their room phones and notified of a fire on the upper floors.  They were in no danger, but asked to evacuate the hotel as a precaution.   Five of the guests decided to use the elevator.  It was discovered later that the elevator only went down about three floors and then for some reason started going up.  It did not stop until it reached the fire floor.  The doors came open and were held open by smoke obscuring the photo cell light beam.  Besides the five guests in the elevator who died of suffocation, firemen noticed that every button had been pushed, probably in a frantic attempt to stop the elevator.

Fires have killed many people, including firemen.  Several New York firemen recently used an elevator when responding to a fire up on the 20th floor.  They pushed 18, but the elevator went right on by the 18th floor.  The doors came open on the 20th floor to an inferno and remained open long enough to kill all the firemen.  The doors then closed and the elevator returned to the lobby.  Hand operated elevators are not exempt.  Some elevator operators have been beaten by people fighting over the controls.  If you have any idea that there might be smoke or fire in your hotel, avoid the elevator like the plague.
JUMPING

It’s important I say something about jumping because so many people do it.  Most are killed or injured in the process.  I cannot tell you whether or not you should jump.  Every fire, although similar, is different.  I can tell you, however, what usually happens to “jumpers”.

If you’re on the 1st floor, you could just OPEN the window and climb out.  From the second floor you could probably make it with a sprained ankle, but you must jump out far enough to clear the building.   Many people hit window sills and ledges on the way down, and they go into cartwheels.  If they don’t land on their head and kill themselves, they’re injured seriously.  If you’re any higher than the 3rd, the chances are you won’t survive the fall.  You would probably be better off fighting the fire.  Nearby buildings seem closer than they really are and many have died trying to jump to a building that looked 5 feet away, but was actually 15 feet away.

Panic is what causes most people to jump.  There was a fire in Brazil a few years ago where 40 people jumped from windows and all 40 died.  Ironically, 36 of those jumped after the fire was out.  Many people have survived by staying put whilst those around them jumped to their death.  If you can resist panic and think clearly, you can use your own best judgment.

## CALLING THE FIRE DEPARTMENT

Believe it or not, most hotels will not call the fire department until they verify whether or not there really is a fire and try to put it out themselves.  Should you call the reception to report a fire, they will always send the bellhop, security guard, or anyone else that’s not busy to investigate.  Hotels are very reluctant to “disturb” their guests and fire engines in the streets are quite embarrassing and tend to draw crowds.

In the New Orleans hotel fire, records show that the fire department received only one call, from a guest in one of the rooms.  The desk had been notified of fire 20 minutes earlier and had sent a security guard to investigate.  His body was later found on the 12th floor about 10 feet from the elevator.

Should you want to report a fire or smell of smoke, ask the hotel operator for an outside line for a local call.  Call the fire department and tell them your room number in case you need to be rescued.  You need not feel embarrassed, that’s what we’re here for.  We would much rather come to a small fire or smoking electrical motel that you smelled than be called 20 minutes later after 6 people have died.  Don’t let hotel “policy” intimidate you into doing otherwise.  The hotel may be a little upset with you, but really ... who gives a damn.  The fire department will be glad you called:  you may have saved many lives.  Besides, it’s a great way for us to meet people!

Well, the rest is up to you.  Only you can condition yourself to react in a hotel emergency.  You can be well prepared by developing the habits we’ve talked about.
