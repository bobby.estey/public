# Math over the Decades

## Teaching Math In 1950

- A logger sells a truckload of lumber for $100. His cost of production is 4/5 of the price. What is his profit?

## Teaching Math In 1960

- A logger sells a truckload of lumber for $100. His cost of production is 4/5 of the price, or $80. What is his profit?

## Teaching Math In 1970

- A logger sells a truckload of lumber for $100. His cost of production is $80. Did he make a profit?

## Teaching Math In 1980

- A logger sells a truckload of lumber for $100. His cost of production is $80 and his profit is $20. Your assignment: Underline the number 20.

## Teaching Math In 1990

- A logger cuts down a beautiful forest because he is selfish and inconsiderate and cares nothing for the habitat of animals or the preservation of our woodlands. He does this so he can make a profit of $20.
- What do you think of this way of making a living? Topic for class participation after answering the question: How did the birds and squirrels feel as the logger cut down their homes? (There are no wrong answers.)

## Teaching Math In 2005

- Un hachero vende una carretada de maderapara $100. El costo de la producción es $80 ...
