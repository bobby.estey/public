# Tips for Handling Telemarketers 

Three Little Words That Work!!

(1)The three little words are: 'Hold On, Please...' 

Saying this, while putting down your phone and walking off (instead of hanging-up immediately) would make each telemarketing call so much more time-consuming that boiler room sales would grind to a halt. 

Then when you eventually hear the phone company's 'beep-beep-beep' tone, you know it's time to go back and hang up your handset, which has efficiently completed its task.

These three little words will help eliminate telephone soliciting. 

(2) Do you ever get those annoying phone calls with no one on the other end? 

This is a telemarketing technique where a machine makes phone calls and records the time of day when a person answers the phone. 

This technique is used to determine the best time of day for a 'real' sales person to call back and get someone at home. 

What you can do after answering, if you notice there is no one there, is to immediately start hitting your # button on the phone, 6 or 7 times as quickly as possible.  This confuses the machine that dialed the call, and it kicks your number out of their system.  Gosh, what a shame not to have your name in their system any longer!!! 

(3) Junk Mail Help: 
When you get 'ads' enclosed with your phone or utility bill, return these 'ads' with your payment. Let the sending companies throw their own junk mail away. 

When you get those 'pre-approved' letters in the mail for everything from credit cards to 2nd mortgages and similar type junk, do not throw away the return envelope. 

Most of these come with postage-paid return envelopes, right?  It costs them more than the regular 41 cents postage, 'IF' and when they receive them back. 

It costs them nothing if you throw them away! The postage was around 50 cents before the last increase and it is according to the weight.  In that case, why not get rid of some of your other junk mail and put it in these cool little, postage-paid return envelopes. 

One of Andy Rooney 's (60 minutes) ideas. 

Send an ad for your local chimney cleaner to American Express. Send a pizza coupon to Citibank.  If you didn't get anything else that day, then just send them their blank application back! 
If you want to remain anonymous, just make sure your name isn't on anything you send them.

You can even send the envelope back empty if you want to just to keep them guessing!  It still costs them 41 cents.

The banks and credit card companies are currently getting a lot of their own junk back in the mail, but folks, we need to OVERWHELM them.  Let's let them know what it's like to get lots of junk mail, and best of all they're paying for it...Twice! 

Let's help keep our postal service busy since they are saying that e-mail is cutting into their business profits, and that's why they need to increase postage costs again.  You get the idea!

If enough people follow these tips, it will work ---- I have been doing this for years, and I get very little junk mail anymore. 

THIS JUST MIGHT BE ONE E-MAIL THAT YOU WILL WANT TO FORWARD TO YOUR FRIENDS