# 4TH OF JULY

Have you ever wondered what happened to the 56 men who signed the Declaration of Independence?

Five signers were captured by the British as traitors and tortured before they died.
Twelve had their homes ransacked and burned. Two lost their sons serving in the Revolutionary Army, another had two sons captured. 
Nine of the 56 fought and died from wounds or hardships of the Revolutionary War.
They signed and they pledged their lives, their fortunes, and their sacred honor.
What kind of men were they:

Twenty-four were lawyers and jurists

Eleven were merchants

Nine were farmers and large plantation owners

Men of means, well educated, but they signed the Declaration of Independence knowing full well that the penalty would be death if they were captured.

Carter Braxton of Virginia, a wealthy planter and trader, saw his Ships swept from the seas by the British Navy.  He sold his home and properties to pay his debts, and died in rags.

Thomas McKeam was so hounded by the British that he was forced to move his family almost constantly.  He served in the Congress without pay, and his family was kept in hiding.  His possessions were taken from him, and poverty was his reward.

Vandals or soldiers looted the properties of Dillery, Hall, Clymer, Walton, Gwinnett, Heyward, Ruttledge, and Middleton.

At the battle of Yorktown, Thomas Nelson, Jr., noted that the British General Cornwallis had taken over the Nelson home for his headquarters.  He quietly urged General George Washington to open fire.  The home was destroyed, and Nelson died bankrupt.

Francis Lewis had his home and properties destroyed.  The enemy jailed his wife, and she died within a few months.

John Hart was driven from his wife's bedside as she was dying.  Their 13 children fled for their lives.  His fields and his gristmill were laid to waste.  For more than a year he lived in forests and caves, returning home to find his wife dead and his children vanished.

Some of us take these liberties so much for granted, but we shouldn't.  So, take a few minutes while enjoying your 4th of July holiday and silently thank these patriots, for the price they paid.

Remember:  freedom is never free!

I hope you will show your support by sending this to as many people as you can, please.  Get the word out that patriotism is NOT a sin, and the Fourth of July has more meaning than taking the day off.
