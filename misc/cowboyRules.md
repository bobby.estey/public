# Cowboy Rules

If these few simple rules below seem offensive to you, you can solve your problem by staying out of the cowboy's life.  He won't miss you for a minute.

1. Don't take a cowboy's picture.
2. Don't expect a cowboy to socialize.  Cowboys are reclusive.
3. Don't expect a cowboy to talk much.  Actions speak louder than words for a cowboy.
4. Cowboys are fiercely independent.  Don't feel threatened by a cowboy's independence or try to undermine it.
5. Don't ask a cowboy questions.
6. A cowboy lives in and for the present.  Certainly don't ask about a cowboy's past.
7. Never shine a light at a cowboy, you might see a rifle with scope pointing back.
8. Never enter a cowboy's camp without permission from the cowboy.
9. Signal your presence to a cowboy's camp from a safe distance.  Rifle bullets travel a long way.
10. Never disturb a cowboy's camp.  Even if his camp consists of just a saddle blanket.
11. Never talk down to a cowboy.
12. Never criticize a cowboy.
13. Never try to impress a cowboy with big talk.
14. Never give a cowboy advice.
15. Never butt into a cowboy's business.
16. Never invade a cowboy's privacy.
17. Never try to boss a cowboy.
18. Never try to marry a cowboy.  Especially if you are a man.
19. Never try to cheat a cowboy.
20. Never commit to a cowboy unless you live up to it.
21. Never try to joke with a cowboy without him knowing that you are joking.  Smile when you say that partner.
22. Never try to explain a joke to a cowboy.  By that time it's too late.
23. Never complain about a cowboy's spurs scratching your furniture.  They make nasty cut marks on human bodies.
24. Never talk negatively about a cowboy's property.  It is the way he wants it, or it wouldn't be that way.
25. Never ask a cowboy for his reasons why.
26. Never try to understand a cowboy's reasons.  Just accept them.
27. Listen carefully when a cowboy speaks.  A cowboy does not like to repeat himself.
28. A cowboy speaks slowly to be easier understood.  Do not misunderstand what a cowboy is saying to you.  Your health might be at stake.
29. When a cowboy asks you to stop doing something, don't make the cowboy ask you again.
30. Do not steal a cowboy's horse unless you can not afford to hire someone to assist in your suicide.
31. Never ask a cowboy how large his spread is.
32. Never attempt to feed or touch a cowboy's animals.
33. Never ask a cowboy to ride his horse.
34. Never try to pet a cowboy's dog.
35. Never foul a cowboy's water hole.
36. Never put a burr under a cowboy's saddle.
37. Treat a cowboy with respect and he will treat you much better.
38. Treat a cowboy with disrespect and he will treat you much worse.
