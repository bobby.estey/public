# Eight Ball Answers

## Answers

|Response Number|Response|
|--|--|
|01|As I See It Yes|Green|
|02|Ask Again Later|Yellow|
|03|Better Not Tell You Now|Yellow|
|04|Cannot Predict Now|Yellow|
|05|Concentrate and Ask Again|Yellow|
|06|Don't Count On It|Red|
|07|It Is Certain|Green|
|08|It Is Decidedly So|Green|
|09|Most Likely|Green|
|10|My Reply Is No|Red|
|11|My Sources Say No|Red|
|12|Outlook Good|Green|
|13|Outlook Not So Good|Red|
|14|Reply Hazy Try Again|Yellow|
|15|Signs Point To Yes|Green|
|16|Very Doubtful|Red|
|17|Without A Doubt|Green|
|18|Yes|Green|
|19|Yes Definitely|Green|
|20|You May Rely On It|Green|

## Response Types

Green: Positive Response (10)
Yellow: Inconclusive Response (5)
Red: Negative Response (5)
