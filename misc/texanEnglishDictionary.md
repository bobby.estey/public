# Texan-English Dictionary

- Just in case the EU decide upon American Engish rather than UK English a few excerpts from the Texas Dictionary of the English Language
- (courtesy of Jim Everhart)

|Word|Explanation|Example|
|--|--|--|
|Yawl|the pronoun of the second person plural|Good to see yawl|
|Ward|a unit of language consisting of one or more spoken sounds|Pardon me, could ah have a ward with yawl|
|Lahr|a prevaricator; one who tells lies|Are yew callin' me a lahr?|
|Waht|the lightest of colours|Yew look waht as a sheet|
|Riot|correct or proper|That's as riot as rain|
|Often|so as to be no longer supported or attached|Now stan still so ah can shoot that apple often yore had|
|Barley|only, just, no more that|Ah can jes barley opne mah eyes|
|Blond|without sight|Love is blond|
|Lacked|was on the verge of or came close to|Ah lacked to died laughin|
|Main|of ugly disposition, nasty|That there is one main man|
|Felons|a substance used to close the cavities in teeth|When ah open mah mouth real wad yawl can see mah two felons|
|Thowed|to cause someone or something to go someplace as if by hurling|Ah'm gonna have yawl thowed in jail|
|Rum|a portion of space within a building|Ah got to go to the restrum|
|Cheer|in this place or spot|Yawl come riot cheer this minute|
|Lard|the deity|Lard only knows what happened|
|Beggar|larger as in size, height, width, amount etc|The beggar they come the harder they fall|
|Thang|a material object without life or conciousness|That don't main a thang|
|Prior|a devout petition to an object of worship|Don't never say a prior with your hat on|
|Suede|dear, beloved, precious|Ain't that jes too suede for wards?|
|Larry|wary, suspicious|Ah would be larry of that if ah was yew|
|Prod|a high opinion of one's own dignity, importance etc|Ah take prod in mah work|
|Far|to discharge a firearm|Stop or ah'm gonna far|
|Tarred|exhausted|Boy, am ah tarred!|They the objective and dative case of thou|Mah country tis of they, suede land of liberty of they ah sang|
|Hem|objective case of he|Ah drawed mah gun on hem|
|Owe|an overwelming feeling of reverence, admiration, fear etc|That there is one ah stand in owe of|
|Thote|the passage in the neck from the mouth to the stomach|Ah got a sore thote|
|Sighting|arousing or stirring up emotions|That was one beg, sighting card game|
|Heidi|an expression of greeting|Heidi, neighbor|
|Sect|afficted with ill health or disease|Ah feel sect to mah stomach|
|Smah|to assume a facial expression indicating pleasure|Smahl and the whole world smahls with yew|
|Consarned|interested or participating|Yawl ain't consarned in this no way|
|Harket|going to the barber|Mah hat never fits after ah get a harket|
|Drank|any liquid taken into the mouth and swallowed|How 'bout a lil drank?|
|Squire|honest and above board|Everything here is fire and squire|
|Ails|other than the person or things implied|Ah only done what anybody ails would do|
|Fair|a distressing emotion aroused by impending danger, evil etc|The only thang we have to fair is fair itsef|
|Tom|any specific point in a day, a month, a year|How come yawl ain't never on tom?|
|Air|the organ of hearing in man|Ah got a airache|
|Truss|reliance on integrity|Don't yawl truss me?|
|Mere|a reflecting surface|Ah jes hate to look at mahsef in the mere|
|Hep|to render assistance|Ain't nobody gonna hep me?|
|Rang|to twist forcibly|Ah'm gonna rang yore neck|
|Farfanger|the first finger next to the thumb|Ah'm holdin' mah nose 'twixt mah thumb an farfinger|
|Markin|a citizen of the United States|Ah am a Markin|
|Thank|to have a judgment or opinion of|Ah hope yawl enjoyed raidin' this page But jes thank of what yew sound lak to a Texan!|
