(function( $ ) {

	$.fn.generalAccordions = function() {
	    
		//ACCORDION
		var cur_stus;

	    //close all on default
	    $('.accordion dd').hide();
	    $('.accordion dt').attr('stus', '');

	    //open default data
	    // $('#accordion dd:eq(0)').slideDown();
	    // $('#accordion dt:eq(0)').attr('stus', 'active');

	    $('.accordion dt').on('click', function () {
	    	//console.log("FUNCTION ONE!");
	    
	        $('.accordion dt').removeClass('dt-selected');
	        
	        cur_stus = $(this).attr('stus');
	        if (cur_stus !== "dt-active") {
	        
	            //reset everything - content and attribute
	            $('.accordion dd').slideUp();
	            $('.accordion dt').attr('stus', '');

	            //then open the clicked data
	            $(this).next().slideDown();
	            $(this).attr('stus', 'dt-active');
	            //$(this).toggleClass("active");
	            $(this).addClass('dt-selected');

	        }
	        //Remove else part if do not want to close the current opened data
	        else {
	            $(this).next().slideUp();
	            $(this).attr('stus', '');
	            //$(this).toggleClass("dt-active");
	            $(this).removeClass('dt-selected');


	        }
	        return false;
	    });

	};

	$.fn.homeAccordions = function() {
	    
		//ACCORDION
		var cur_stus;

		if($(window).width() < 768) {

			jQuery('.home-selected').find('.slide-container').height(268);

		} else {

			var slide_height = ($(window).height() - 354);

			jQuery('.home-selected').find('.slide-container').height(slide_height);

			jQuery('.home-selected').height(slide_height);

			//on window resize
			jQuery(window).bind('resize', function () { 

				//Reassign the variable to the slide_height
				var slide_height = jQuery(window).height() - 354;
				
				jQuery('.home-selected').find('.slide-container').css({
					'height' : slide_height
				});

				jQuery('.home-selected').height(slide_height);


			});


		}

	    //close all on default
	    $('.accordion dd').hide();
	    $('.accordion dt').attr('stus', '');


	    $('.accordion .click-to-trigger').on('click', function () {
	        
	    		if(jQuery(window).width() > 768 || jQuery(window).height() > 768) {
	    			var slide_height = ($(window).height() - 354);
	    		} else {
	    			var slide_height = 350;
	    		}
	    		
	        	$( ".home-blurb" ).animate({
					opacity: 0
				}, 50, function() {
					// Animation complete.
					
				});

				$( ".black-overlay" ).toggleClass('black-overlay-none');

	        	$( ".black-overlay" ).animate({
					opacity: 1
				}, 300, function() {
					// Animation complete.
					
				});


	
					 //reset everything - content and attribute
		            $( ".home-selected" ).find('.slide-container').animate({
						height: "50px"
					}, 300, function() {
						// Animation complete.
						
					});

			
           		 	$('.home-selected').css({height : '50px'});
           
            	

	            $('.accordion dt').removeClass('home-selected');
	            $('.accordion dt').attr('stus', '');

	            //If mobile or tables size
	            if($(window).width() < 768 && jQuery(window).height() < 481) {

	            	$(this).closest('dt').find('.slide-container').animate({
						height: 350
					}, 300, function() {
						// Animation complete.
					});

	            } else {

		            //then open the clicked data
		            $(this).closest('dt').find('.slide-container').animate({
						height: slide_height
					}, 300, function() {
						// Animation complete.
					});

	       		}

	            $(this).closest('dt').find('.home-blurb').animate({
					opacity: 1
				}, 300, function() {
					// Animation complete.
				});

	            $(this).closest('dt').height(slide_height);
	          
	            
	            $(this).closest('dt').toggleClass('home-selected');
	            
	        
	        return false;

	    });

	};
 
}( jQuery ));
	