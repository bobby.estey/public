# Military Terminology

|NAVY / USMC|ARMY|AIR FORCE|
|--|--|--|
|Head|Latrine|Powder Room|
|Rack|Bunk|Single with ruffle and duvet|
|Mess Deck / Chow Hall|Mess Hall / Mess Tent|Dining Facility/Cafe|
|Cookie|Mess Cook|Contract Chef|
|Coffee/Mud|Cup of Joe|Vanilla Latte|
|Bug Juice|Kool-Aid|Shirley Temple|
|Utilities|BDU's|Casual Wear|
|Seaman/Private|Private|Bobby or Jimmy|
|Chief/Gunny|Sergeant|Bob or Jim|
|Captain/Skipper|Colonel|Robert or James|
|Captain's Mast|Article 15|Time Out|
|Berthing/Barracks|Barracks|Apartment|
|Skivvies / U-Trau|Underwear|Tee Shirt & Panties|
|Thrown in the Brig|Put in confinement|Grounded|
|Zoom Bag|Flight Suit|Business Casual|
|Cover|Beret|Optional|
|Ship's Store / PX|PX|AAFEs - Shopping Mall|
|TAD|TDY|PCS with family|
|Cruise / Afloat|Deploy|Huh?|
|Ground Grabbers|Athletic Shoes|Flip-Flops|
|Boondockers|Jump Boots|Berkenstocks|
|Low Quarters|Low Quarters|Patent Leather Pumps|
|SEAL|Special Forces|Librarian|
|Hoo-Yah ! (SEALS)/Hoo-Rah ! (USMC)|Hoo-Ah !|Uh-Oh !|
|MRE|MRE|Happy Meal|
|Grinder|Drill|FieldWhat?|
|Ge-Dunk|Snack Bar|Chucky Cheese|
|Midshipman|Cadet|Debutant|
|Hard-Core|Strak|Way Too Serious|
