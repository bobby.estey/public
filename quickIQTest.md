# Quick IQ Test

- This test will give you some gauge of your mental flexibility and creativity.  In the years since the development of this test, few people have been able to solve more than half of the 24 items on the FIRST TRY.  Many, however, report getting answers long after the test has been set aside.  Take this test as a
personal challenge!

1.  26 = L of the A
2.  64 = S on a C B
3.   7 = W of the A W
4.  29 = D in F in a L Y
5.  1001 = A N
6.  1000 = W that a P is W
7.  12 = Son of the Z
8.  11 = P on a F T
9.  52 = C in a D
10. 57 = H V
11.  9 = P in the S S
12.  5 = D in a Z C
13. 88 = K on a P
14. 60 = S in a M
15. 13 = S on the A F
16.  1 = W on a U
17. 32 = D F at which W F
18.  4 = Q in a G
19. 18 = H on the G C
20. 52 = W in a Y
21. 90 = D in a R A
22.  8 = S on a S S
23.  3 = B. M. (S. H. T. R.)
24.  4 = S in a Y

- Answers Below

1.  26 = L of the A           Letters of the Alphabet
2.  64 = S on a C B           Squares on a Checker Board
3.   7 = W of the A W         Wonders of the Ancient World
4.  29 = D in F in a L Y      Days in February in a Leap Year
5.  1001 = A N                Arabian Nights
6.  1000 = W that a P is W    Words that a Picture is Worth
7.  12 = Son of the Z         Signs of the Zodiac
8.  11 = P on a F T           Players on a Footbal Team
9.  52 = C in a D             Cards in a Deck
10. 57 = H V                  Heinz Varieties
11.  9 = P in the S S         Planets in the Solar System
12.  5 = D in a Z C           Digits in a Zip Code
13. 88 = K on a P             Keys on a Piano
14. 60 = S in a M             Seconds in a Minute
15. 13 = S on the A F         Stripes on the American Flag
16.  1 = W on a U             Wheel on a Unicycle
17. 32 = D F at which W F     Degress Farenheit at which Water Freezes
18.  4 = Q in a G             Quarters in a Game
19. 18 = H on the G C         Holes on the Golf Course
20. 52 = W in a Y             Weeks in a Year
21. 90 = D in a R A           Degrees in a Right Angle
22.  8 = S on a S S           Sides on a Stop Sign
23.  3 = B. M. (S. H. T. R.)  Blind Mice (See How They Run)
24.  4 = S in a Y             Seasons in a Year
