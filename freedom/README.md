#  Freedom

- Here in alphabetical order are the final resting places of many brave young men
- Europeans call the United States arrogant and President Obama apologizes to Europeans for American Arrogance
 
- The American Cemetery at Aisne-Marne, France. A total of 2,289 of US Military dead
  
![00.jpg](00.jpg)

- The American Cemetery at Ardennes, Belgium . A total of 5,329 of US Military dead

![01.jpg](01.jpg)

- The American Cemetery at Brittany, France. A total of 4,410 of US Military dead

![02.jpg](02.jpg)

- Brookwood, England American Cemetery. A total of 468 of US Military dead

![03.jpg](03.jpg)

- Cambridge, England. A total of 3,812 of US Military dead

![04.jpg](04.jpg)

- Epinal, France American Cemetery. A total of 5,525 of US Military dead

![05.jpg](05.jpg)

- Flanders Field, Belgium . A total of 368 of US Military

![06.jpg](06.jpg)

- Florence, Italy. A total of 4,402 of US Military dead

![07.jpg](07.jpg)

- Henri-Chapelle, Belgium. A total of 7,992 of US Military dead

![08.jpg](08.jpg)

- Lorraine, France. A total of 10,489 of US Military dead

![09.jpg](09.jpg)

- Luxembourg, Luxembourg . A total of 5,076 of US Military dead

![10.jpg](10.jpg)

- Meuse-Argonne. A total of 14,246 of US Military dead

![11.jpg](11.jpg)

- Netherlands, Netherlands . A total of 8,301 of US Military dead

![12.jpg](12.jpg)

- Normandy, France. A total of 9,387 of US Military dead

![13.jpg](13.jpg)

- Oise-Aisne, France. A total of 6,012 of US Military dead

![14.jpg](14.jpg)

- Rhone, France. A total of 861 of US Military dead

![15.jpg](15.jpg)

- Sicily, Italy. A total of 7,861 of US Military dead

![16.jpg](16.jpg)

- Somme, France. A total of 1,844 of US Military dead

![17.jpg](17.jpg)

- St. Mihiel, France. A total of 4,153 of US Military dead

![18.jpg](18.jpg)

- Suresnes, France. A total of 1,541 of US Military dead

![19.jpg](19.jpg)

## Conclusion

- When this much blood and treasure was expended to liberate France, Belgium, Luxembourg and The Netherlands
- President Obama should not dishonor the sacred memory of these brave heroes, by standing on European soil and apologizing for American arrogance

## THE COUNT IS 104,366   
